import os
from flask import Flask
from flask import request

from ocr import get_value

app = Flask(__name__)


@app.route('/')
def help():
    return 'Python OCR Service for CHATET.in'


@app.route('/ocr')
def ocr():
    url = request.args.get('url')
    return str(get_value(url))


port = int(os.environ.get("PORT", 5000))
app.run(host='0.0.0.0', port=port)
