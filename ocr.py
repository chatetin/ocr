import urllib.request as rq
import cv2
import pytesseract
import numpy as np
import re

def url_to_image(url):
    # download image,convert to a NumPy array,and read it into opencv
    resp = rq.urlopen(url)
    img = np.asarray(bytearray(resp.read()), dtype="uint8")
    img = cv2.imdecode(img, cv2.IMREAD_COLOR)
    return img

def find_amounts(text):
    amounts = re.findall(r'\d+[\.,]\d+\b', text)
    floats = [int(amount.replace(",", "").replace(".", "")) for amount in amounts]
    unique = list(dict.fromkeys(floats))
    return unique

def get_value(url):
    try:
        k = url_to_image(url)
        t = pytesseract.image_to_string(k)
        ams = find_amounts(t)
        return max(ams)
    except:
        return -1