from .ocr import get_value

def test_ocr_with_good_url():
    v = get_value("https://media-cdn.tripadvisor.com/media/photo-s/10/b5/66/fb/receipt.jpg")
    assert v == 315810

def test_ocr_with_bad_url():
    v = get_value("badurl")
    assert v == -1